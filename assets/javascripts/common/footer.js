const Footer = {
  handleSubmenu: function () {
    const menus = document.querySelectorAll('[data-action="toggle-menu-footer"]');

    menus.length > 0 && menus.forEach((menu) => {
      menu.addEventListener('click', () => {
        menu.classList.toggle('-open', !menu.classList.contains('-open'));
      });
    });
  },
  init: function () {
    const _this = this;

    _this.handleSubmenu();
  },
};

export default Footer;
