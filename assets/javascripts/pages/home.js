import setFullbanner from './home/1_fullbanner';
import setIconsHorizontal from './home/horizontal_icons_slider.js';
import setCarousel from '../components/carousel';
import setBrandsCarousel from './home/brands_carousel';
import setQuotationVerticalSwiper from './home/3_quotation';


//addImports

import setTopBar from '../components/topBar';
import handleConditionalLazy from '../components/conditionalLazy';

const Home = {
  init: function () {
    setTopBar();
    handleConditionalLazy();

    setFullbanner()
		setIconsHorizontal();
		setCarousel();
		setBrandsCarousel();
		setQuotationVerticalSwiper();
		
		
		//calls
  },
};

window.addEventListener('DOMContentLoaded', () => {
  Home.init();
});
