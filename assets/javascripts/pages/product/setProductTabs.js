import { slideToggle } from '../../components/utilities';

export default function setProductTabs() {
  const descriptionDropdowns = document.querySelectorAll('[data-description-dropdown]');

  descriptionDropdowns.forEach((tab) => {
    tab.addEventListener('click', () => {
        tab.classList.toggle('-open');
        const contentWrapper = tab.parentElement.querySelector('.content');
        const content = contentWrapper.querySelector('.text');
        slideToggle(contentWrapper, content)
    })
  })
}
