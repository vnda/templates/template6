export default function setPopupSizes() {
  const popup = document.querySelector('[data-size-popup]');
  const togglers = document.querySelectorAll('[data-action="toggle-popup"]');

  if (popup && togglers.length > 0) {
    togglers.forEach((button) => {
      button.addEventListener('click', () => {
        popup.classList.toggle('-open');
      });
    });
  }
}
