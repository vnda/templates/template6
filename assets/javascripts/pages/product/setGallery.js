import Swiper from 'swiper';
import { Manipulation, Navigation, Pagination, Zoom, Thumbs } from 'swiper/modules';

function handleCustomPagination() {
  const mainImagesContainer = document.querySelector('[data-main-slider]');
  const thumbsContainer = document.querySelector('[data-slider-thumbs]');
  if (!thumbsContainer || !mainImagesContainer) return;

  const thumbs = thumbsContainer.querySelectorAll('[data-slider-thumbs] .swiper-slide');

  if (thumbs.length > 0) {
    const halfContainerHeight = mainImagesContainer.offsetHeight / 2;

    mainImagesContainer.addEventListener('scroll', function () {
      const productImages = mainImagesContainer.querySelectorAll('[data-image]');
      const containerTop = mainImagesContainer.scrollTop;
      const containerBottom = containerTop + mainImagesContainer.offsetHeight;

      productImages.forEach((image) => {
        const elementTop = image.offsetTop + halfContainerHeight;
        const elementBottom = elementTop + image.offsetHeight;

        const activeThumb = thumbsContainer.querySelector(`[data-image-index="${image.getAttribute('id')}"]`);

        if (elementBottom > containerTop && elementTop < containerBottom && activeThumb) {
          thumbs.forEach((el) => el.classList.remove('-active'));
          activeThumb.classList.add('-active');
        }
      });
    });

    thumbs.forEach((thumb) => {
      thumb.addEventListener('click', () => {
        const relativeSlide = thumb.getAttribute('data-image-index');
        if (relativeSlide) {
          const elementToScroll = mainImagesContainer.querySelector(`#${relativeSlide}`);
          if (elementToScroll) {
            mainImagesContainer.scroll({
              top: elementToScroll.offsetTop,
              left: 0,
              behavior: 'smooth',
            });
          }
        }
      });
    });

    thumbs[0].classList.add('-active');
  }
}

export default function setGallery(Product) {
  const productImagesContainer = document.querySelector('[data-product-images]');

  const thumbs = new Swiper(Product.thumbsSlider.element, {
    modules: [Manipulation],
    direction: 'vertical',
    slidesPerView: 'auto',
    slidesPerGroup: 1,
    spaceBetween: 16,
    watchSlidesProgress: true,
    watchOverflow: true,
  });

  const main = new Swiper(Product.mainSlider.element, {
    modules: [Manipulation, Navigation, Pagination, Zoom, Thumbs],
    slidesPerView: 1.1,
    spaceBetween: 4,
    speed: 1000,
    watchSlidesProgress: true,
    watchOverflow: true,
    centeredSlides: true,
    navigation: {
      nextEl: '[data-main-slider] .swiper-button-next',
      prevEl: '[data-main-slider] .swiper-button-prev',
    },
    pagination: {
      el: '.product-images .swiper-pagination',
      clickable: true,
      type: 'bullets',
    },
    breakpoints: {
      992: {
        direction: 'vertical',
        slidesPerView: 1.2,
        spaceBetween: 8,
        centeredSlides: false,
      },
    },
    zoom: {
      maxRatio: 1.5,
      zoomedSlideClass: '-zoomed',
    },
    on: {
      init: () => {
        window.innerWidth >= 992 && handleCustomPagination();
      },
      zoomChange: (swiper, scale, imageEl, slideEl) => {
        setTimeout(() => {
          if (slideEl.classList.contains('-zoomed')) {
            productImagesContainer.classList.add('-zoomed');
          } else {
            productImagesContainer.classList.remove('-zoomed');
          }
        }, 500);
      },
    },
  });

  Product.thumbsSlider.swiper = thumbs;
  Product.mainSlider.swiper = main;
}
