import Swiper from 'swiper';
import { Pagination, Autoplay } from 'swiper/modules';

export default function setIconsHorizontal() {
  const sections = document.querySelectorAll('[data-icons-horizontal]');

  sections.forEach((section) => {
    const carousel = section.querySelector('.swiper');
    const pagination = section.querySelector('.swiper-pagination');

    const iconsSwiper = new Swiper(carousel, {
      modules: [Pagination, Autoplay],
      slidesPerView: 1,
      spaceBetween: 24,
      watchOverflow: true,
      preloadImages: false,
      centerInsufficientSlides: true,
      speed: 1000,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
        pauseOnMouseEnter: true,
      },
      pagination: {
        el: pagination,
        type: 'bullets',
        clickable: true,
      },
      breakpoints: {
        768: {
          spaceBetween: 30,
          slidesPerView: 'auto',
        },
      },
    });

    iconsSwiper.autoplay.stop();

    const eventType = window.mobile ? 'scroll' : 'mousemove';
    window.addEventListener(eventType, iconsSwiper.autoplay.start, { once: true });
  });
}
