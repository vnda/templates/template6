import { addAsset } from './utilities';

// ===============================================================
// NEWSLETTER POPUP
// ===============================================================
export const NewsletterComponent = {
  root: document.querySelector('#component-popup-newsletter-root'),
  settings: window.popupNewsletterSettings || false,
  loaded: false,

  setPopupNewsletter: function () {
    const { settings, root } = NewsletterComponent;

    // Define propriedades do componente
    let maxWidth = settings.maxWidth;
    let imageUrl = settings.imageUrl;
    let title = settings.title;
    let description = settings.description;
    let extraDescription = settings.extraDescription;
    let subdomain = settings.subdomain;
    let successMessage = settings.success;

    // Inicia o componente
    const componentNewsletterPopup = new Vnda.Component.NewsletterPopup({
      maxWidth: maxWidth,
      maxHeight: 500,
      imageUrl: imageUrl,
      imagePosition: 'left',
      popupPosition: 'center',
      title: title,
      description: description,
      extraDescription: extraDescription,
      textBtnSubmit: 'Enviar',
      classBtnSubmit: 'button-newsletter',
      formKey: `${subdomain}-newsletter`,
      hasNameField: false,
      hasLastNameField: false,
      hasDateOfBirthField: false,
      hasPhoneField: false,
      successMessage: successMessage,
      delay: 500,
      frequency: '7',
      language: 'pt-BR',
      placeholders: {
        // firstName: 'Nome',
        // lastName: 'Sobrenome',
        // phone: 'Telefone',
        email: 'Digite seu e-mail',
      },
    });

    // Renderiza o componente e marca como carregado
    componentNewsletterPopup.render(root);
    NewsletterComponent.loaded = true;
  },

  loadPopupNewsletter: function () {
    if (!NewsletterComponent.loaded) {
      const { settings } = NewsletterComponent;
      addAsset(settings.script, NewsletterComponent.setPopupNewsletter);
      addAsset(settings.styles);
    }
  },

  init: function () {
    const { root, settings } = this;

    if (!root || !settings) return;

    const eventType = window.innerWidth <= 1024 ? 'scroll' : 'mousemove';
    window.addEventListener(
      eventType,
      () => {
        NewsletterComponent.loadPopupNewsletter();
      },
      { once: true }
    );
  },
};

// ===============================================================
// PREÇO
// ===============================================================
// Carrega o componente de preço quando um product-block ou product-infos entra em tela
export const PriceComponent = {
  script: window.priceComponent || false,
  loaded: false,

  init: function () {
    if (!PriceComponent.script) return;

    const productContainers = document.querySelectorAll('[data-product-box]');

    if (productContainers.length === 0) return;

    const observer = new IntersectionObserver(
      (entries) => {
        for (const entry of entries) {
          if (entry.isIntersecting) {
            if (!PriceComponent.loaded) {
              addAsset(PriceComponent.script);
              PriceComponent.loaded = true;
            }
            observer.disconnect();
            break;
          }
        }
      },
      { threshold: 0.1 }
    );

    productContainers.forEach((product) => {
      observer.observe(product);
    });
  },
};
