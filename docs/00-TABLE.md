
<!-- _class: table-of-contents -->

# Orientações de cadastro

![Logo Vnda](../images/prints/vnda.svg)

## [Tabela de conteúdos](#1)

- ### [GERAL](#2)    - [1 POPUP DE NEWSLETTER](#2)    - [2 CARRINHO - FRETE GRÁTIS](#2)    - [3 SUGESTÕES CARRINHO](#2)    - [4 BANNER FAIXA TOPO](#2)    - [5 LOGO PRINCIPAL](#2)    - [6 MENU PRINCIPAL](#2)    - [7 BANNER DO SUBMENU](#2)    - [8 RODAPÉ LOGO](#2)    - [9 REDES SOCIAIS](#2)    - [10 RODAPÉ ATENDIMENTO](#2)    - [11 RODAPÉ TEXTO](#2)    - [12 RODAPÉ SELOS](#2)    - [13 MENU FOOTER](#2)    - [14 ASSINATURA - CNPJ](#2) - ### [HOME](#3)    - [1 FULLBANNER PRINCIPAL](#3)    - [2 SLIDER DE ICONES HORIZONTAL](#3)    - [3 CARROSSEL DE PRODUTOS SUPERIOR](#3)    - [4 BANNERS DE CATEGORIAS](#3)    - [5 CARROSSEL DE PRODUTOS SUPERIOR](#3)    - [6 BANNERS ESPELHADOS (até 6)](#3)    - [7 DEPOIMENTOS VERTICAL](#3)    - [8 BANNER SOBRE](#3)    - [9 INSTAGRAM - TÍTULO E BOTÃO](#3) - ### [TAG](#4)    - [1 TAG FULLBANNER](#4)    - [2 SLIDER DE CATEGORIAS](#4)    - [3 FILTRO](#4)    - [4 TAG FLAG](#4) - ### [PRODUTO](#5)    - [1 IMAGENS](#5)    - [2 DESCRIÇÃO](#5)    - [3 VARIANTES](#5)    - [4 TAG MODELO](#5)    - [5 GUIA DE MEDIDAS](#5)    - [6 TAG BANNER](#5)    - [7 BANNERS DE PRODUTO](#5)    - [8 SEÇÃO COMPRE JUNTO (até 3 produtos)](#5)    - [9 PRODUTOS RELACIONADOS](#5) - ### [SOBRE NOS](#6)    - [1 FULLBANNER TOPO](#6)    - [2 BANNER CONTEÚDO - TEXTO SUPERIOR](#6)    - [3 BANNER IMAGEM E TEXTO](#6)    - [4 BANNER HORIZONTAL](#6)    - [5 BANNERS DEPOIMENTO](#6) - ### [ATENDIMENTO](#7)    - [1 FULLBANNER TOPO](#7)    - [2 FORMULÁRIO DE CONTATO](#7)    - [3 TEXTO FORMULÁRIO](#7)    - [4 BANNER DE INFORMAÇÕES DA LOJA (até 4)](#7)    - [5 SEÇÃO DE ABAS](#7)    - [6 SEÇÃO DE ABAS - TÍTULO](#7) - ### [TERMOS](#8)    - [1 ABAS LATERAIS](#8) - ### [ONDE-ENCONTRAR](#9)    - [1 FULLBANNER TOPO](#9)    - [2 LOCAIS](#9) 

***
