- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/02-home.png)

# HOME

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |


&nbsp;

## FULLBANNER PRINCIPAL

**_Informações:_**

| Dúvida                | Instrução                                                        |
| --------------------- | ---------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                          |
| **Onde será exibido** | Banner principal abaixo do header, ocupa 100% da largura da tela |
| **Cadastro exemplo**  | [Admin](https://template6.vnda.dev/admin/midias/editar?id=1) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo               | Funcional?          | Orientação                                                            |
| ------------------- | ------------------- | --------------------------------------------------------------------- |
| **Imagem**          | :large_blue_circle: | Dimensão sugerida Desktop: 2200x1012 pixels, Mobile: 1000x1460 pixels |
| **Título**          | :black_circle:      | Alt da imagem                                                         |
| **Subtítulo**       | :large_blue_circle: | Título do botão e posição do texto do banner. Opções a baixo!         |
| **Descrição**       | :large_blue_circle: | Título e descrição do banner                                          |
| **Externo?**        | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba                |
| **URL**             | :large_blue_circle: | Link de direcionamento                                                |
| **Posição Desktop** | :black_circle:      | `home-banner-principal-1`                                        |
| **Posição Mobile**  | :black_circle:      | `home-banner-principal-mobile-1`                                 |
| **Cor**             | :large_blue_circle: |  Cor dos textos                                                       |

&nbsp;

**_Exemplo de subtítulo:_**

```md
CALL TO ACTION | left-center
```

**_Exemplo de descrição:_**

```md
\#\#\# UPPER TITLE
\#\# Título do Banner

Descrição do banner alinhado à esquerda.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

**_Posições do botão possíveis para colocar no campo subtítulo:_**

- left-top
- left-center
- left-bottom

- center-top
- center-center
- center-bottom

- right-top
- right-center
- right-bottom


&nbsp;

## SLIDER DE ICONES HORIZONTAL

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Seção de slider de ícones                                     |
| **Cadastro exemplo**  | [Admin](https://template6.vnda.dev/admin/midias/editar?id=49) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                                             |
| ------------- | ------------------- | ------------------------------------------------------ |
| **Imagem**    | :black_circle:      | Dimensão sugerida 60x60 pixels                         |
| **Título**    | :black_circle:      | Alt da imagem                                          |
| **Subtítulo** | :large_blue_circle: | Título do ícone                                        |
| **Descrição** | :large_blue_circle: | Descrição do ícone                                     |
| **Externo?**  | :no_entry:          |                                                        |
| **URL**       | :no_entry:          |                                                        |
| **Posição**   | :black_circle:      | `home-icones-horizonal-slider`                  |
| **Cor**       | :no_entry:          |                                                        |


&nbsp;

## CARROSSEL DE PRODUTOS SUPERIOR

**_Informações:_**

| Dúvida                | Instrução                                                                       |
| --------------------- | ------------------------------------------------------------------------------- |
| **Onde cadastrar**    | Tags                                                                            |
| **Onde será exibido** | Carrossel de produtos, abaixo das tabs de categoria                             |
| **Cadastro exemplo**  | [Admin](https://template6.vnda.dev/admin/tags/editar?id=home-produtos) |

&nbsp;

**_Informações sobre os campos_**

| Campo         | Funcional?          | Orientação                                   |
| ------------- | ------------------- | -------------------------------------------- |
| **Nome**      | :black_circle:      | `home-produtos-1`                       |
| **Título**    | :large_blue_circle: | Título do carrossel                          |
| **Subtítulo** | :no_entry:          |                                              |
| **Descrição** | :no_entry:          |                                              |
| **Tipo**      | :no_entry:          |                                              |
| **Imagem**    | :no_entry:          |                                              |


&nbsp;

## BANNERS DE CATEGORIAS

**_Informações:_**

| Dúvida                | Instrução                                                          |
| --------------------- | ------------------------------------------------------------------ |
| **Onde cadastrar**    | Banners                                                            |
| **Onde será exibido** | Seção com dois banners lado a lado |
| **Cadastro exemplo**  | [Admin](https://template6.vnda.dev/admin/midias/editar?id=67)      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                                              |
| ------------- | ------------------- | ------------------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 1000x667 pixels                       |
| **Título**    | :black_circle:      | Alt da imagem                                           |
| **Subtítulo** | :large_blue_circle: | Texto do botão. Só será exibido se o banner possuir link |
| **Descrição** | :large_blue_circle: | Upper title, título e descrição curta. Exemplo a baixo! |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em nova aba  |
| **URL**       | :large_blue_circle: | Link de direcionamento                                  |
| **Posição**   | :black_circle:      | `home-banner-categorias`                                |
| **Cor**       | :no_entry:          |                                                         |

&nbsp;

**_Exemplo de descrição:_**

```md
\#\#\# UPPER TITLE

\#\# Título do banner

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

## CARROSSEL DE PRODUTOS SUPERIOR

**_Informações:_**

| Dúvida                | Instrução                                                                       |
| --------------------- | ------------------------------------------------------------------------------- |
| **Onde cadastrar**    | Tags                                                                            |
| **Onde será exibido** | Carrossel de produtos, abaixo das tabs de categoria                             |
| **Cadastro exemplo**  | [Admin](https://template6.vnda.dev/admin/tags/editar?id=home-produtos) |

&nbsp;

**_Informações sobre os campos_**

| Campo         | Funcional?          | Orientação                                   |
| ------------- | ------------------- | -------------------------------------------- |
| **Nome**      | :black_circle:      | `home-produtos-2`                       |
| **Título**    | :large_blue_circle: | Título do carrossel                          |
| **Subtítulo** | :no_entry:          |                                              |
| **Descrição** | :no_entry:          |                                              |
| **Tipo**      | :no_entry:          |                                              |
| **Imagem**    | :no_entry:          |                                              |


&nbsp;

## BANNERS ESPELHADOS (até 6)

***Informações:***

| Dúvida                          | Instrução                                                         |
| ------------------------------- | ----------------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                           |
| **Onde será exibido**           | Banners espelhados, abaixo dos banners de linha                   |
| **Cadastro exemplo em staging** | [Admin](https://template6.vnda.dev/)                          |

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação                                                |
| ------------- | ------------------- | --------------------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 1000x625 pixels                         |
| **Título**    | :black_circle:      | Alt da imagem                                             |
| **Subtítulo** | :large_blue_circle: | Texto do botão. Só será exibido se uma url for cadastrada |
| **Descrição** | :large_blue_circle: | Texto do banner. Exemplo abaixo                           |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba    |
| **URL**       | :large_blue_circle: | Link de direcionamento                                    |
| **Posição**   | :black_circle:      | `home-banner-espelhado`                            |
| **Cor**       | :no_entry:          |                                                           |

**_Exemplo de descrição:_**

```md
\#\# TÍTULO DA SEÇÃO

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

## DEPOIMENTOS VERTICAL

***Informações:***

| Dúvida                          | Instrução                                                       |
| ------------------------------- | --------------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                         |
| **Onde será exibido**           | Seção de depoimentos                                            |
| **Cadastro exemplo em staging** | [Admin](https://template6.vnda.com.br/admin/midias/editar?id=5) |

&nbsp;

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação                                                                         |
| ------------- | ------------------- | ---------------------------------------------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 150x150 pixels                                                   |
| **Título**    | :black_circle:      | Alt da imagem                                                                      |
| **Subtítulo** | :large_blue_circle: | Nome e subtítulo, separados por `\|`. Ex: `Maria da Silva \| Corretora de imóveis` |
| **Descrição** | :large_blue_circle: | Depoimento                                                                         |
| **Externo?**  | :no_entry:          |                                                                                    |
| **URL**       | :no_entry:          |                                                                                    |
| **Posição**   | :black_circle:      | `home-depoimento-vertical`                                                  |
| **Cor**       | :no_entry:          |                                                                                    |


&nbsp;

## BANNER SOBRE

**_Informações:_**

| Dúvida                          | Instrução                                                    |
| ------------------------------- | ------------------------------------------------------------ |
| **Onde cadastrar**              | Banners                                                      |
| **Onde será exibido**           | Banners sobre, abaixo do banner de marcas                    |
| **Cadastro exemplo**            | [Admin](https://template6.vnda.dev/admin/midias/editar?id=47)  |

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                                                |
| ------------- | ------------------- | --------------------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 200x200 pixels                          |
| **Título**    | :black_circle:      | Alt da imagem                                             |
| **Subtítulo** | :large_blue_circle: | Texto do botão. Só será exibido se uma url for cadastrada |
| **Descrição** | :large_blue_circle: | Texto do banner. Exemplo abaixo                           |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba    |
| **URL**       | :large_blue_circle: | Link de direcionamento                                    |
| **Posição**   | :black_circle:      | `home-sobre-nos`                                   |
| **Cor**       | :no_entry:          |                                                           |

**_Exemplo de descrição:_**

```md
\#\# Sobre nós

Lorem ipsum dolor sit amet consectetur. Aliquet eu in placerat accumsan nec eget odio. Platea tempus odio sed morbi sit elit. Duis praesent ante est consequat velit. Pellentesque sem eu dictum potenti augue eu interdum. Enim blandit ultricies cras sit tellus. Commodo quisque aliquam tellus in ipsum interdum penatibus.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).
## INSTAGRAM - TÍTULO E BOTÃO

**_Informações:_**

| Dúvida                          | Instrução                                           |
| ------------------------------- | --------------------------------------------------- |
| **Onde cadastrar**              | Banners                                             |
| **Onde será exibido**           | Título da seção do instagem                         |
| **Cadastro exemplo em staging** | [Admin](https://template6.vnda.dev)             |

&nbsp;

***Orientações sobre os campos:***

| Campo         | Funcional?     | Orientação             |
| ------------- | -------------- | ---------------------- |
| **Imagem**    | :large_blue_circle: | Ícone exibido ao lado do título. Dimensões sugeridas de 64x64 pixels |
| **Título**    | :black_circle:      | Utilizado para controle interno |
| **Subtítulo** | :black_circle:      | @ da marca \| texto do botão. Ex.: "@marcainstagram \| Seguir +" |
| **Descrição** | :no_entry:          |                        |
| **Externo?**  | :no_entry:          |                        |
| **URL**       | :black_circle:      | Link para redirecionamento |
| **Posição**   | :black_circle:      | `home-instagram-texto` |
| **Cor**       | :no_entry:          |                        |

&nbsp;

**_Observações:_**

As imagens utilizadas no ambiente de desenvolvimento e pré produção, são apenas para demarcar a seção.
Para está seção aparecer em produção é necessário realizar a integração com o instagram. Caso deseje está opção, entrar em contato com o atendimento.
***
