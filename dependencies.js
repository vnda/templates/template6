const path = require('path');
const current_project = path.basename(__dirname);
const basePath = `../${current_project}/node_modules`;

module.exports = {
  modules: [
    {
      'vanilla-lazyload': {
        files: [`${basePath}/vanilla-lazyload/dist/lazyload.min.js`],
      },
    },
  ],
};
